package com.example.Loginapplication;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.interfaces.PBEKey;
import javax.crypto.spec.SecretKeySpec;

@RequiresApi(api = Build.VERSION_CODES.O)
public class MainActivity extends AppCompatActivity {
    Base64.Decoder decoder = Base64.getDecoder();
    String A5BdQFtLyZJ = "U3VwZXJEaWZmaWN1bHRVc2VybmFtZQ==";
    String strEncrypted = "v93+eHSMFsaMx/pQDOJl2av+4huQV4Ytmxwvv41Lm0M=";
    private EditText Name;
    private EditText Passwd;
    private Button Login;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Name = (EditText)findViewById(R.id.etName);
        Passwd = (EditText)findViewById(R.id.etPassword);
        Login = (Button)findViewById(R.id.button);
        textView = (TextView)findViewById(R.id.textView);
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = Name.getText().toString();
                String b = Passwd.getText().toString();
                if (a.isEmpty() || b.isEmpty()){
                    Toast.makeText(MainActivity.this, "You can't submit an empty form ya crazy animal!", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        checkUsername(a, b);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void checkUsername(String a, String b) throws Exception {
        byte[] barr = decoder.decode(A5BdQFtLyZJ);
        String data = new String(barr);
        if (a.equals(data)){
            checkPassword(b);
        } else {
            Toast.makeText(MainActivity.this, "Your credentials are incorrect, try again!", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkPassword(String a) throws Exception {
        if (Z2V0TUQ1(a).equals("fb99bbd2b633aea125c37b6260469af6")){
            Toast.makeText(MainActivity.this, "Correct password bro!", Toast.LENGTH_SHORT).show();
            Login.setEnabled(false);
            showFlag(a);
        } else {
            Toast.makeText(MainActivity.this, "Your credentials are incorrect, try again!", Toast.LENGTH_SHORT).show();
        }
    }

    private void showFlag(String a) throws Exception {
        String decryptKey = a + "123456";
        String flag = decrypt(decryptKey, strEncrypted);
        textView.setText(flag);
    }

    public static String Z2V0TUQ1(String input)
    {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            StringBuilder hashtext = new StringBuilder(no.toString(16));
            while (hashtext.length() < 32) {
                hashtext.insert(0, "0");
            }
            return hashtext.toString();
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    String hint = "VjFjNU1VbEhUbWhpYVdRd1NVZGtNVnBZVG5wSlNGSnZXbE5DZDFsWVRucGtNamw1V2tOM1oxTllVV2RrTWtaNlNVVjBiRnBZUW14amFXUjZTVWMxZVV4cFFYaE9VMEpwV1ZkT2NrbEhiSFZKUkVsM1RWUlpQUT09";
    public static String decrypt(String strKey, String strEncrypted) throws Exception{
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] cipherBytes = decoder.decode(strEncrypted);
        SecretKeySpec secretKeySpec = new SecretKeySpec(strKey.getBytes("UTF-8"),"AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        byte[] decryptedFlag = cipher.doFinal(cipherBytes);
        String flag = new String(decryptedFlag, StandardCharsets.UTF_8);
        return flag;
    }
}